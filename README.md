# Jooq with graalvm

## Dev:

**PostgreSQL**: `docker run --rm -it -e POSTGRES_PASSWORD=jooq -e POSTGRES_USER=jooq -e POSTGRES_DB=jooq -p 5432:5432 -v (pwd)/src/database/:/docker-entrypoint-initdb.d/ --name postgres-foo postgres:12.3-alpine`

Inside the compilation container: 

```shell
$ # Previous gu install native-image should have been made…
$ ./mvnw --version
Apache Maven 3.6.3 (cecedd343002696d0abb50b32b541b8a6ba2883f)
Maven home: /usr/local/Cellar/maven/3.6.3_1/libexec
Java version: 11.0.7, vendor: GraalVM Community, runtime: /Users/kevin/.jabba/jdk/graalvm-ce-java11@20.1.0/Contents/Home
Default locale: en_FR, platform encoding: UTF-8
OS name: "mac os x", version: "10.15.6", arch: "x86_64", family: "mac"

$ ./mvnw clean jooq-codegen:generate package
[INFO] Scanning for projects...
[INFO]
[INFO] -----------------< com.gitlab.davinkevin:picocli-jooq >-----------------
[INFO] Building picocli-jooq 1.0-SNAPSHOT
[INFO] --------------------------------[ jar ]---------------------------------
[INFO]
[INFO] --- maven-clean-plugin:2.5:clean (default-clean) @ picocli-jooq ---
[INFO]
[INFO] --- jooq-codegen-maven:3.13.2:generate (default-cli) @ picocli-jooq ---
[INFO] No <inputCatalog/> was provided. Generating ALL available catalogs instead.
[INFO] License parameters
[INFO] ----------------------------------------------------------
[INFO]   Thank you for using jOOQ and jOOQ's code generator
[INFO]
[INFO] Database parameters
[INFO] ----------------------------------------------------------
[INFO]   dialect                : POSTGRES
[INFO]   URL                    : jdbc:postgresql://localhost:5432/jooq
[INFO]   target dir             : /Users/kevin/Workspace/gitlab.com/davinkevin/picocli-jooq/target/generated-sources/jooq
[INFO]   target package         : com.gitlab.davinkevin.picoclijooq.database
[INFO]   includes               : [.*]
[INFO]   excludes               : []
[INFO]   includeExcludeColumns  : false
[INFO] ----------------------------------------------------------
[INFO]
[INFO] JavaGenerator parameters
[INFO] ----------------------------------------------------------
[INFO]   annotations (generated): false
[INFO]   annotations (JPA: any) : false
[INFO]   annotations (JPA: version):
[INFO]   annotations (validation): false
[INFO]   comments               : true
[INFO]   comments on attributes : true
[INFO]   comments on catalogs   : true
[INFO]   comments on columns    : true
[INFO]   comments on keys       : true
[INFO]   comments on links      : true
[INFO]   comments on packages   : true
[INFO]   comments on parameters : true
[INFO]   comments on queues     : true
[INFO]   comments on routines   : true
[INFO]   comments on schemas    : true
[INFO]   comments on sequences  : true
[INFO]   comments on tables     : true
[INFO]   comments on udts       : true
[INFO]   sources                : true
[INFO]   sources on views       : true
[INFO]   daos                   : false
[INFO]   deprecated code        : true
[INFO]   global references (any): true
[INFO]   global references (catalogs): true
[INFO]   global references (keys): true
[INFO]   global references (links): true
[INFO]   global references (queues): true
[INFO]   global references (routines): true
[INFO]   global references (schemas): true
[INFO]   global references (sequences): true
[INFO]   global references (tables): true
[INFO]   global references (udts): true
[INFO]   indexes                : true
[INFO]   instance fields        : true
[INFO]   interfaces             : false
[INFO]   interfaces (immutable) : false
[INFO]   javadoc                : true
[INFO]   keys                   : true
[INFO]   links                  : true
[INFO]   pojos                  : false
[INFO]   pojos (immutable)      : false
[INFO]   queues                 : true
[INFO]   records                : true
[INFO]   routines               : true
[INFO]   sequences              : true
[INFO]   sequenceFlags          : true
[INFO]   table-valued functions : true
[INFO]   tables                 : true
[INFO]   udts                   : true
[INFO]   relations              : true
[INFO] ----------------------------------------------------------
[INFO]
[INFO] Generation remarks
[INFO] ----------------------------------------------------------
[INFO]
[INFO] ----------------------------------------------------------
[INFO] Generating catalogs      : Total: 1
[INFO]

@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@  @@        @@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@        @@@@@@@@@@
@@@@@@@@@@@@@@@@  @@  @@    @@@@@@@@@@
@@@@@@@@@@  @@@@  @@  @@    @@@@@@@@@@
@@@@@@@@@@        @@        @@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@        @@        @@@@@@@@@@
@@@@@@@@@@    @@  @@  @@@@  @@@@@@@@@@
@@@@@@@@@@    @@  @@  @@@@  @@@@@@@@@@
@@@@@@@@@@        @@  @  @  @@@@@@@@@@
@@@@@@@@@@        @@        @@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@  @@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@
@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@@  Thank you for using jOOQ 3.13.2

[INFO] ARRAYs fetched           : 0 (0 included, 0 excluded)
[INFO] Enums fetched            : 0 (0 included, 0 excluded)
[INFO] Packages excluded
[INFO] Routines fetched         : 0 (0 included, 0 excluded)
[INFO] Sequences fetched        : 0 (0 included, 0 excluded)
[INFO] Tables fetched           : 2 (2 included, 0 excluded)
[INFO] No schema version is applied for catalog . Regenerating.
[INFO]
[INFO] Generating catalog       : DefaultCatalog.java
[INFO] ==========================================================
[INFO] Generating schemata      : Total: 1
[INFO] No schema version is applied for schema public. Regenerating.
[INFO] Generating schema        : Public.java
[INFO] ----------------------------------------------------------
[INFO] UDTs fetched             : 0 (0 included, 0 excluded)
[INFO] Generating tables
[INFO] Adding foreign key       : item_podcast_id_fkey (public.item.podcast_id) referencing podcast_pkey
[INFO] Synthetic primary keys   : 0 (0 included, 0 excluded)
[INFO] Overriding primary keys  : 4 (0 included, 4 excluded)
[INFO] Generating table         : Item.java [input=item, output=item, pk=item_pkey]
[INFO] Embeddables fetched      : 0 (0 included, 0 excluded)
[INFO] Indexes fetched          : 0 (0 included, 0 excluded)
[INFO] Generating table         : Podcast.java [input=podcast, output=podcast, pk=podcast_pkey]
[INFO] Tables generated         : Total: 617.544ms
[INFO] Generating table references
[INFO] Table refs generated     : Total: 620.566ms, +3.022ms
[INFO] Generating Keys
[INFO] Keys generated           : Total: 625.427ms, +4.86ms
[INFO] Generating Indexes
[INFO] Skipping empty indexes
[INFO] Generating table records
[INFO] Generating record        : ItemRecord.java
[INFO] Generating record        : PodcastRecord.java
[INFO] Table records generated  : Total: 644.49ms, +19.063ms
[INFO] Domains fetched          : 0 (0 included, 0 excluded)
[INFO] Generation finished: public: Total: 664.088ms, +19.597ms
[INFO]
[INFO] Removing excess files
[INFO]
[INFO] --- maven-resources-plugin:2.6:resources (default-resources) @ picocli-jooq ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] Copying 1 resource
[INFO]
[INFO] --- maven-compiler-plugin:3.8.1:compile (default-compile) @ picocli-jooq ---
[INFO] Changes detected - recompiling the module!
[INFO] Compiling 11 source files to /Users/kevin/Workspace/gitlab.com/davinkevin/picocli-jooq/target/classes
[INFO]
[INFO] --- maven-resources-plugin:2.6:testResources (default-testResources) @ picocli-jooq ---
[INFO] Using 'UTF-8' encoding to copy filtered resources.
[INFO] skip non existing resourceDirectory /Users/kevin/Workspace/gitlab.com/davinkevin/picocli-jooq/src/test/resources
[INFO]
[INFO] --- maven-compiler-plugin:3.8.1:testCompile (default-testCompile) @ picocli-jooq ---
[INFO] Changes detected - recompiling the module!
[INFO]
[INFO] --- maven-surefire-plugin:2.12.4:test (default-test) @ picocli-jooq ---
[INFO]
[INFO] --- maven-jar-plugin:2.4:jar (default-jar) @ picocli-jooq ---
[INFO] Building jar: /Users/kevin/Workspace/gitlab.com/davinkevin/picocli-jooq/target/picocli-jooq-1.0-SNAPSHOT.jar
[INFO]
[INFO] --- native-image-maven-plugin:20.1.0:native-image (default) @ picocli-jooq ---
[INFO] ImageClasspath Entry: info.picocli:picocli:jar:4.5.1:compile (file:///Users/kevin/.m2/repository/info/picocli/picocli/4.5.1/picocli-4.5.1.jar)
[INFO] ImageClasspath Entry: org.postgresql:postgresql:jar:42.2.14:compile (file:///Users/kevin/.m2/repository/org/postgresql/postgresql/42.2.14/postgresql-42.2.14.jar)
[INFO] ImageClasspath Entry: org.jooq:jooq:jar:3.13.2:compile (file:///Users/kevin/.m2/repository/org/jooq/jooq/3.13.2/jooq-3.13.2.jar)
[INFO] ImageClasspath Entry: org.reactivestreams:reactive-streams:jar:1.0.2:compile (file:///Users/kevin/.m2/repository/org/reactivestreams/reactive-streams/1.0.2/reactive-streams-1.0.2.jar)
[INFO] ImageClasspath Entry: javax.xml.bind:jaxb-api:jar:2.3.1:compile (file:///Users/kevin/.m2/repository/javax/xml/bind/jaxb-api/2.3.1/jaxb-api-2.3.1.jar)
[INFO] ImageClasspath Entry: javax.activation:javax.activation-api:jar:1.2.0:compile (file:///Users/kevin/.m2/repository/javax/activation/javax.activation-api/1.2.0/javax.activation-api-1.2.0.jar)
[INFO] ImageClasspath Entry: javax.persistence:javax.persistence-api:jar:2.2:compile (file:///Users/kevin/.m2/repository/javax/persistence/javax.persistence-api/2.2/javax.persistence-api-2.2.jar)
[INFO] ImageClasspath Entry: com.gitlab.davinkevin:picocli-jooq:jar:1.0-SNAPSHOT (file:///Users/kevin/Workspace/gitlab.com/davinkevin/picocli-jooq/target/picocli-jooq-1.0-SNAPSHOT.jar)
[INFO] Executing: /Users/kevin/.jabba/jdk/graalvm-ce-java11@20.1.0/Contents/Home/bin/native-image -cp /Users/kevin/.m2/repository/info/picocli/picocli/4.5.1/picocli-4.5.1.jar:/Users/kevin/.m2/repository/org/postgresql/postgresql/42.2.14/postgresql-42.2.14.jar:/Users/kevin/.m2/repository/org/jooq/jooq/3.13.2/jooq-3.13.2.jar:/Users/kevin/.m2/repository/org/reactivestreams/reactive-streams/1.0.2/reactive-streams-1.0.2.jar:/Users/kevin/.m2/repository/javax/xml/bind/jaxb-api/2.3.1/jaxb-api-2.3.1.jar:/Users/kevin/.m2/repository/javax/activation/javax.activation-api/1.2.0/javax.activation-api-1.2.0.jar:/Users/kevin/.m2/repository/javax/persistence/javax.persistence-api/2.2/javax.persistence-api-2.2.jar:/Users/kevin/Workspace/gitlab.com/davinkevin/picocli-jooq/target/picocli-jooq-1.0-SNAPSHOT.jar -H:+TraceClassInitialization --no-fallback -H:+ReportExceptionStackTraces --allow-incomplete-classpath -H:ReflectionConfigurationFiles=./classes/reflection-config.json -H:Class=com.gitlab.davinkevin.picoclijooq.JooqInsert -H:Name=picocli-jooq
Build on Server(pid: 26778, port: 51602)
[picocli-jooq:26778]    classlist:     847.90 ms,  4.61 GB
[picocli-jooq:26778]        (cap):   1,835.58 ms,  4.61 GB
[picocli-jooq:26778]        setup:   2,008.47 ms,  4.61 GB
[picocli-jooq:26778]     (clinit):     416.87 ms,  5.02 GB
[picocli-jooq:26778]   (typeflow):   7,597.62 ms,  5.02 GB
[picocli-jooq:26778]    (objects):   9,740.16 ms,  5.02 GB
[picocli-jooq:26778]   (features):     592.52 ms,  5.02 GB
[picocli-jooq:26778]     analysis:  18,971.08 ms,  5.02 GB
[picocli-jooq:26778]     universe:     344.50 ms,  5.02 GB
[picocli-jooq:26778]      (parse):   1,167.49 ms,  5.19 GB
[picocli-jooq:26778]     (inline):   1,002.09 ms,  5.19 GB
[picocli-jooq:26778]    (compile):   5,968.96 ms,  4.81 GB
[picocli-jooq:26778]      compile:   8,992.24 ms,  4.81 GB
[picocli-jooq:26778]        image:   1,808.96 ms,  4.81 GB
[picocli-jooq:26778]        write:     504.96 ms,  4.89 GB
[picocli-jooq:26778]      [total]:  33,526.00 ms,  4.89 GB
[INFO] ------------------------------------------------------------------------
[INFO] BUILD SUCCESS
[INFO] ------------------------------------------------------------------------
[INFO] Total time:  37.822 s
[INFO] Finished at: 2020-09-11T17:27:28+02:00
[INFO] ------------------------------------------------------------------------
```
