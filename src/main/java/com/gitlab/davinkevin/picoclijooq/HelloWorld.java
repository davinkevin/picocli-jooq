package com.gitlab.davinkevin.picoclijooq;

import picocli.CommandLine;

import static picocli.CommandLine.*;

/**
 * Created by kevin on 12/01/2020
 */
@Command
public class HelloWorld implements Runnable {

    public static void main(String[] args) {
        new CommandLine(new HelloWorld()).execute(args);
    }

    public void run() {
        System.out.println("Hello Kevin");
    }
}
