package com.gitlab.davinkevin.picoclijooq;

import com.oracle.svm.core.annotate.Substitute;
import com.oracle.svm.core.annotate.TargetClass;

/**
 * Created by kevin on 11/09/2020
 */
@TargetClass(className = "org.jooq.impl.DefaultRecordMapper", innerClass = "ProxyMapper")
final class ProxyMapper_Substitute {
    @Substitute
    private Object proxy() {
        throw new UnsupportedOperationException("Can't work with GraalVM native");
    }
}
