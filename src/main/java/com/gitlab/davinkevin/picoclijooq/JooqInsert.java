package com.gitlab.davinkevin.picoclijooq;

import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import picocli.CommandLine;

import java.sql.Connection;
import java.sql.DriverManager;
import java.time.OffsetDateTime;
import java.util.UUID;

import static com.gitlab.davinkevin.picoclijooq.database.Tables.PODCAST;
import static picocli.CommandLine.Command;

/**
 * Created by kevin on 12/01/2020
 */
@Command
public class JooqInsert implements Runnable {

    public static void main(String[] args) {
        new CommandLine(new JooqInsert()).execute(args);
    }

    public void run() {
        var conn = createConnection();

        var query = DSL.using(conn, SQLDialect.POSTGRES);

        var id = UUID.randomUUID();
        query.insertInto(PODCAST)
                .set(PODCAST.ID, id)
                .set(PODCAST.DESCRIPTION, "foo")
                .set(PODCAST.SIGNATURE, id.toString())
                .set(PODCAST.LAST_UPDATE, OffsetDateTime.now())
                .set(PODCAST.TITLE, "podcast " + id)
                .set(PODCAST.URL, "https://gitlab.com/davinkevin/picocli-jooq/" + id)
                .execute();

        System.out.println("podcast with id " + id + " is inserted");

    }

    private Connection createConnection() {
        var userName = "jooq";
        var password = "jooq";
        var url = "jdbc:postgresql://localhost:5432/jooq";

        // Connection is the only JDBC resource that we need
        // PreparedStatement and ResultSet are handled by jOOQ, internally
        try {
            return DriverManager.getConnection(url, userName, password);
        } catch (Exception e) {
            throw new RuntimeException("Error during connection creation", e);
        }
    }

}
